<?php

namespace Drupal\commerce_purchase_order\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the customer approved condition for purchase orders.
 *
 * @CommerceCondition(
 *   id = "commerce_purchase_order_auth",
 *   label = @Translation("User Authorized"),
 *   display_label = @Translation("Limit by field: Purchase Orders Authorized"),
 *   category = @Translation("Customer"),
 *   entity_type = "commerce_order",
 * )
 */
class PurchaseOrderCustomerApproved extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $customer = $order->getCustomer();

    if ($customer->hasField('field_purchase_orders_authorized')) {
      if ($customer->get('field_purchase_orders_authorized')->isEmpty()) {
        return FALSE;
      }
      else {
        /** @var \Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem $fieldItem */
        $fieldItem = $customer->get('field_purchase_orders_authorized')->first();
        return (bool) $fieldItem->value;
      }
    }
    return FALSE;
  }

}
