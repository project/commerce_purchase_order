<?php

namespace Drupal\commerce_purchase_order\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the Purchase Order payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "purchase_order",
 *   label = @Translation("Purchase Order"),
 *   create_label = @Translation("New Purchase Order"),
 *
 * )
 */
class PurchaseOrder extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $label = NULL;
    if ($payment_method->hasField('po_number')) {
      $label = $payment_method->po_number->value;
    }
    elseif ($payment_method->hasField('po_file') && !$payment_method->po_file->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $payment_method->get('po_file')->entity;
      $label = $file->getFilename();
    }

    $placeholders = [
      '@label' => $label,
    ];
    return $this->t('Purchase Order# @label', $placeholders);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['po_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Purchase Order Number'))
      ->setDescription(t('The number assigned to your purchase order.'))
      ->setRequired(FALSE);

    $fields['po_file'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Purchase order file'))
      ->setRequired(FALSE)
      ->setDescription(t('The purchase order file'))
      ->setSetting('target_type', 'file');

    return $fields;
  }

}
