<?php

namespace Drupal\commerce_purchase_order\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "purchase_order_gateway",
 *   label = "Purchase Orders",
 *   display_label = "Purchase Orders",
 *    forms = {
 *     "receive-payment" =
 *   "Drupal\commerce_payment\PluginForm\PaymentReceiveForm",
 *     "add-payment-method" =
 *   "Drupal\commerce_purchase_order\PluginForm\PurchaseOrder\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"purchase_order"},
 *   payment_type = "payment_purchase_order"
 * )
 */
class PurchaseOrderGateway extends OnsitePaymentGatewayBase implements OnsitePaymentGatewayInterface, HasPaymentInstructionsInterface, SupportsVoidsInterface, SupportsRefundsInterface, ContainerFactoryPluginInterface {

  /**
   * Injected module service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Injected file service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected FileUsageInterface $fileUsage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->fileUsage = $container->get('file.usage');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditCardTypes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'instructions' => [
        'value' => '',
        'format' => 'plain_text',
      ],
      'file_upload' => FALSE,
      'file_extensions' => 'pdf',
      'limit_open' => 1.0,
      'user_approval' => TRUE,
      'payment_method_types' => ['purchase_order'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['limit_open'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit maximum open purchase orders'),
      '#default_value' => $this->configuration['limit_open'],
      '#description' => $this->t('During the authorization state at checkout, the transaction is denied if the number of unpaid payments exceeds this number.'),
      '#min' => 1,
      '#step' => 1.0,
      '#size' => 3,
    ];
    $form['user_approval'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Purchase order users require approval in the user account settings.'),
      '#default_value' => $this->configuration['user_approval'],
    ];
    $form['instructions'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Payment instructions'),
      '#description' => $this->t('Shown the end of checkout, after the customer has placed their order.'),
      '#default_value' => $this->configuration['instructions']['value'],
      '#format' => $this->configuration['instructions']['format'],
    ];

    // Flag if private system is set and ready for use.
    $private_file_system = (bool) PrivateStream::basePath();

    $form['file_upload'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow file upload for purchase order payment method'),
      '#default_value' => $this->configuration['file_upload'],
      '#disabled' => !$private_file_system,
    ];

    if (!$private_file_system) {
      $form['file_upload']['#prefix'] = '<strong>' . $this->t('You need to setup Drupal private file system to use file upload option') . '</strong>';
    }

    // Make the extension list a little more human-friendly by comma-separation.
    $extensions = str_replace(' ', ', ', $this->configuration['file_extensions'] ?? 'pdf');
    $form['file_extensions'] = [
      '#type' => 'textfield',
      '#title' => t('Allowed file extensions'),
      '#default_value' => $extensions,
      '#description' => t('Separate extensions with a space or comma and do not include the leading dot.'),
      '#element_validate' => [[static::class, 'validateExtensions']],
      '#weight' => 1,
      '#maxlength' => 256,
      '#states' => [
        'visible' => [
          ':input[name="configuration[purchase_order_gateway][file_upload]"]' => ['checked' => TRUE],
        ],
      ],
      '#disabled' => !$private_file_system && !$this->moduleHandler->moduleExists('file'),
    ];

    return $form;
  }

  /**
   * Wrapper for FileItem::validateExtensions.
   */
  public static function validateExtensions($element, FormStateInterface $form_state) {
    FileItem::validateExtensions($element, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['instructions'] = $values['instructions'];
      $this->configuration['limit_open'] = (int) $values['limit_open'];
      $this->configuration['user_approval'] = (bool) $values['user_approval'];
      $this->configuration['file_extensions'] = $values['file_extensions'];

      // If private system is not set, always set file upload to false.
      $this->configuration['file_upload'] = PrivateStream::basePath() && $values['file_upload'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->value;
    $operations = [];
    $operations['receive'] = [
      'title' => $this->t('Receive'),
      'page_title' => $this->t('Receive payment'),
      'plugin_form' => 'receive-payment',
      'weight' => -99,
      'access' => $payment_state == 'authorized',
    ];
    $operations['void'] = [
      'title' => $this->t('Void'),
      'page_title' => $this->t('Void payment'),
      'plugin_form' => 'void-payment',
      'access' => $payment_state == 'authorized',
      'weight' => 90,
    ];
    $operations['refund'] = [
      'title' => $this->t('Refund'),
      'page_title' => $this->t('Refund payment'),
      'plugin_form' => 'refund-payment',
      'access' => in_array($payment_state, ['completed', 'partially_refunded']),
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    if (!empty($this->configuration['instructions']['value'])) {
      $instructions = [
        '#type' => 'processed_text',
        '#text' => $this->configuration['instructions']['value'],
        '#format' => $this->configuration['instructions']['format'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $this->authorizePayment($payment);
    $this->assertAuthorized($payment);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, ?Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorized']);
    // If not specified, use the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorized']);
    $payment->setState('voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    if (empty($payment_details['number'])) {
      if ($this->configuration['file_upload'] && empty($payment_details['file'])) {
        throw new HardDeclineException(sprintf('$payment_details must contain the %s key.', 'number or file'));
      }
      throw new HardDeclineException(sprintf('$payment_details must contain the %s key.', 'number'));
    }

    // Not re-usable because we will store the PO number in the method.
    $payment_method->setReusable(FALSE);

    if (!empty($payment_details['number']) && $payment_method->hasField('po_number')) {
      $payment_method->set('po_number', $payment_details['number']);
    }

    if (!empty($payment_details['file']) && $payment_method->hasField('po_file')) {
      $payment_method->set('po_number', end($payment_details['file']));
    }

    $payment_method->setExpiresTime(strtotime("+60 day"));
    $payment_method->save();

    // Upon saving payment method, mark usage of the file by this module
    // and saved payment method.
    if ($file = $payment_method->get('po_file')->entity) {
      $this->fileUsage->add($file, 'commerce_purchase_order', $payment_method->getEntityTypeId(), $payment_method->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete payment method, but also a file attached if there is any.
    if (
      $payment_method->hasField('po_file')
      && !$payment_method->get('po_file')->isEmpty()
    ) {
      $file = $payment_method->get('po_file')->entity;
      $file->delete();
    }

    // There is no remote system.  These are only stored locally.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, ?Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Authorizes payment based on settings in the gateway configuration.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to authorize.
   */
  protected function authorizePayment(PaymentInterface $payment) {
    $customer = $payment->getOrder()->getCustomer();
    if (
      $this->configuration['user_approval']
      && $customer->hasField('field_purchase_orders_authorized')) {
      if ($customer->get('field_purchase_orders_authorized')->isEmpty()) {
        $user_approved = FALSE;
      }
      else {
        /** @var \Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem $fieldItem */
        $fieldItem = $customer->get('field_purchase_orders_authorized')
          ->first();
        $user_approved = $fieldItem->value;
      }
    }
    else {
      // There is no user approval.
      $user_approved = TRUE;
    }
    if (!$user_approved) {
      $this->messenger()
        ->addWarning($this->t('Please contact us about using purchase orders for checkout.'));
    }
    $user_po_methods = $this->entityTypeManager->getStorage('commerce_payment_method')
      ->loadByProperties([
        'uid' => $customer->id(),
        'type' => 'purchase_order',
      ]);
    if (!empty($user_po_methods)) {
      $user_po_method_ids = [];
      foreach ($user_po_methods as $method) {
        $user_po_method_ids[] = $method->id();
      }
      $payment_query = $this->entityTypeManager->getStorage('commerce_payment')
        ->getQuery();
      // This query is internal - not displayed to users.
      $payment_query->accessCheck(FALSE);
      $payment_query->condition('payment_method', $user_po_method_ids, 'IN')
        ->condition('state', 'authorized')
        ->count();
      $open_po_count = $payment_query->execute();
    }
    else {
      $open_po_count = 0;
    }
    if ($user_approved && ($open_po_count < $this->configuration['limit_open'])) {
      $payment->setState('authorized');
      $payment->setAuthorizedTime($this->time->getRequestTime());
    }
  }

  /**
   * Asserts that the payment successfully authorized.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @throws \Drupal\commerce_payment\Exception\HardDeclineException
   *   Thrown when the payment method did not authorize.
   */
  protected function assertAuthorized(PaymentInterface $payment) {
    if ($payment->getState()->value != 'authorized') {
      throw new HardDeclineException('The purchase order failed to authorized.  Please contact a site administrator.');
    }
  }

}
