<?php

namespace Drupal\commerce_purchase_order\PluginForm\PurchaseOrder;

use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodFormBase;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PrivateStream;

/**
 * PaymentMethodAddForm for Purchase Order.
 */
class PaymentMethodAddForm extends PaymentMethodFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => 'purchase_order',
    ];
    $form['payment_details'] = $this->buildPurchaseOrderForm($form['payment_details'], $form_state);
    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    // The payment method form is customer facing. For security reasons
    // the returned errors need to be more generic.
    // The Purchase Order Gateway will handle processing the form values.
    try {
      $payment_gateway_plugin->createPaymentMethod($payment_method, $values['payment_details']);
    }
    catch (DeclineException $e) {
      $this->logger->warning($e->getMessage());
      throw new DeclineException('We encountered an error processing your payment method. Please verify your details and try again.');
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException('We encountered an unexpected error processing your payment method. Please try again later.');
    }
  }

  /**
   * Builds the purchase order form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built form.
   */
  protected function buildPurchaseOrderForm(array $element, FormStateInterface $form_state) {
    $payment_gateway = $this->entity->getPaymentGateway();
    $plugin = $payment_gateway->getPluginConfiguration();
    $allow_file = FALSE;
    if (PrivateStream::basePath()) {
      // If private files are configured, check our setting.
      $allow_file = $plugin['file_upload'] ?? FALSE;
    }

    $element['#attributes']['class'][] = 'purchase-order-form';
    $element['number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Purchase Order number'),
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => !$allow_file,
      '#maxlength' => 19,
      '#size' => 20,
    ];

    $element['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Purchase Order file'),
      '#size' => 50,
      '#required' => FALSE,
      '#access' => $allow_file,
      '#upload_location' => 'private://purchase-orders',
      '#progress_message' => $this->t('Please wait...'),
      '#attributes' => ['class' => ['file-import-input']],
      '#upload_validators' => [
        'FileExtension' => ['extensions' => $plugin['file_extensions'] ?? 'pdf'],
      ],
    ];

    return $element;
  }

  /**
   * Handles the submission of the billing profile form.
   *
   * @param array $element
   *   The billing profile form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitBillingProfileForm(array $element, FormStateInterface $form_state) {
    $billing_profile = $element['#entity'];
    $form_display = EntityFormDisplay::collectRenderDisplay($billing_profile, 'default');
    $form_display->extractFormValues($billing_profile, $element, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_method->setBillingProfile($billing_profile);
  }

}
