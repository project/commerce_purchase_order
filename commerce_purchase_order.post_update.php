<?php

/**
 * @file
 * Post-update hooks for commerce_purchase_order.
 */

use Drush\Drush;

/**
 * Alters stored payments states for issue 3184883.
 */
function commerce_purchase_order_post_update_awaiting_payment() {
  // First completed -> authorized.
  // Second paid -> completed.
  $database = Drupal::database();
  $po_updated = $database->update('commerce_payment')
    ->fields(['state' => 'authorized'])
    ->condition('type', 'payment_purchase_order')
    ->condition('state', 'completed')
    ->execute();
  Drupal::logger('commerce_purchase_order')->info('Payment state changed from completed to authorized for @count payments', ['@count' => $po_updated]);
  if (class_exists('Drush\Drush')) {
    Drush::output()->writeln(dt('Payment state changed from completed to authorized for @count payments', ['@count' => $po_updated]));
  }

  $po_updated = $database->update('commerce_payment')
    ->fields(['state' => 'completed'])
    ->condition('type', 'payment_purchase_order')
    ->condition('state', 'paid')
    ->execute();
  Drupal::logger('commerce_purchase_order')->info('Payment state changed from paid to completed for @count payments', ['@count' => $po_updated]);
  if (class_exists('Drush\Drush')) {
    Drush::output()->writeln(dt('Payment state changed from paid to completed for @count payments', ['@count' => $po_updated]));
  }
}
