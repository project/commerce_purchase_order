<?php

namespace Drupal\Tests\commerce_purchase_order\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;

/**
 * Tests the checkout of an order.
 *
 * @group commerce_purchase_order
 */
class PurchaseOrderGatewayConditionTest extends CommerceBrowserTestBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;


  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_product',
    'commerce_order',
    'commerce_cart',
    'commerce_checkout',
    'commerce_payment',
    'commerce_purchase_order',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge(
      [
        'administer commerce_checkout_flow',
      ],
      parent::getAdministratorPermissions()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->store->set('billing_countries', ['US']);
    $this->store->save();

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::create([
      'id' => 'po',
      'label' => 'PO',
      'plugin' => 'purchase_order_gateway',
      'configuration' => [
        'limit_open' => 3,
        'user_approval' => 1,
        'instructions' => [
          'value' => 'Sample payment instructions.',
          'format' => 'plain_text',
        ],
        'file_upload' => FALSE,
        'file_extensions' => 'txt',
      ],
      'conditions' => [
        [
          'plugin' => 'commerce_purchase_order_auth',
          'configuration' => [],
        ],
      ],
      'conditionOperator' => 'AND',
    ]);
    $payment_gateway->save();

    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 9.99,
        'currency_code' => 'USD',
      ],
    ]);

    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'My product',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);

    $this->placeBlock('commerce_cart');
    $this->placeBlock('commerce_checkout_progress');
  }

  /**
   * Tests anonymous and authenticated checkout.
   */
  public function testAnonymousPurchaseOrderCheckout() {
    $this->drupalLogout();
    $this->drupalGet($this->product->toUrl());
    $this->submitForm([], 'Add to cart');
    $this->assertSession()->pageTextContains('1 item');
    $cart_link = $this->getSession()->getPage()->findLink('your cart');
    $cart_link->click();
    $this->submitForm([], 'Checkout');
    $this->assertSession()->pageTextNotContains('Order Summary');

    $this->assertCheckoutProgressStep('Log in');
    $this->submitForm([], 'Continue as Guest');
    $this->assertCheckoutProgressStep('Order information');
    $this->assertSession()
      ->pageTextContains('There are no payment gateways available for this order');
  }

  /**
   * Tests authenticated & unauthorized checkout.
   */
  public function testUnauthorizedUserCheckout() {
    // Authenticated - not authorized.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->product->toUrl());
    $this->submitForm([], 'Add to cart');
    $this->assertSession()->pageTextContains('1 item');
    $cart_link = $this->getSession()->getPage()->findLink('your cart');
    $cart_link->click();
    $this->submitForm([], 'Checkout');
    $this->assertSession()->pageTextContains('Order Summary');
    $this->assertCheckoutProgressStep('Order information');
    $this->assertSession()
      ->pageTextContains('There are no payment gateways available for this order');
  }

  /**
   * Tests authenticated & authorized checkout.
   */
  public function testAuthorizedUserCheckout() {
    // Authenticated - authorized.
    $this->drupalLogout();
    $authorizedUser = $this->drupalCreateUser([], 'Authorized');
    $this->assertTrue($authorizedUser->hasField('field_purchase_orders_authorized'), 'Missing field `field_purchase_orders_authorized`');
    $authorizedUser->field_purchase_orders_authorized->setValue(1);
    $authorizedUser->save();
    $this->drupalLogin($authorizedUser);
    $this->drupalGet($this->product->toUrl());
    $this->submitForm([], 'Add to cart');
    $this->assertSession()->pageTextContains('1 item');
    $cart_link = $this->getSession()->getPage()->findLink('your cart');
    $cart_link->click();
    $this->submitForm([], 'Checkout');
    $this->assertSession()->pageTextContains('Order Summary');
    $this->assertCheckoutProgressStep('Order information');
    $this->submitForm([
      'payment_information[add_payment_method][payment_details][number]' => 'c99',
      'payment_information[add_payment_method][billing_information][address][0][address][given_name]' => 'Authorized',
      'payment_information[add_payment_method][billing_information][address][0][address][family_name]' => 'Johnson',
      'payment_information[add_payment_method][billing_information][address][0][address][organization]' => 'Example',
      'payment_information[add_payment_method][billing_information][address][0][address][address_line1]' => '9 Drupal Ave',
      'payment_information[add_payment_method][billing_information][address][0][address][postal_code]' => '94043',
      'payment_information[add_payment_method][billing_information][address][0][address][locality]' => 'Mountain View',
      'payment_information[add_payment_method][billing_information][address][0][address][administrative_area]' => 'CA',
    ], 'Continue to review');
    $this->assertCheckoutProgressStep('Review');
    $this->assertSession()->pageTextContains('Contact information');
    $this->assertSession()->pageTextContains('Payment information');
    $this->assertSession()->pageTextContains('Order Summary');
    $this->submitForm([], 'Pay and complete purchase');
    $this->assertSession()->pageTextContains('Your order number is 1');
    $this->assertSession()
      ->pageTextContainsOnce('Sample payment instructions.');
    $this->assertSession()->pageTextContains('0 items');
  }

  /**
   * Asserts the current step in the checkout progress block.
   *
   * @param string $expected
   *   The expected value.
   */
  protected function assertCheckoutProgressStep($expected) {
    $current_step_node = $this->getSession()->getPage()->find(
      'css',
      '.checkout-progress--step__current'
    );
    if (!$current_step_node instanceof NodeElement) {
      $this->fail('Checkout progress element is not present.');
    }
    $current_step = $current_step_node->getText();
    $this->assertEquals($expected, $current_step);
  }

}
